﻿using System.Collections.Generic;

namespace Kwiatkowski.MNIST.Interfaces
{
    public interface IImage
    {
        int ImageID { get; set; }
        int? Evaluate { get; set; }
        int Label { get; set; }
        List<float> Feature { get; set; }
    }
}