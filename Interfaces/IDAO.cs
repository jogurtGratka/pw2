﻿using System.Collections.Generic;

namespace Kwiatkowski.MNIST.Interfaces
{
    public interface IDAO
    {
        IEnumerable<IImage> GetAllImages();
        IModel GetModel();
        IImage CreateNewImage();
        void AddImage(IImage image);
    }
}