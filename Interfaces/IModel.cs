﻿using Microsoft.MSR.CNTK.Extensibility.Managed;

namespace Kwiatkowski.MNIST.Interfaces
{
    public interface IModel
    {
        string Name { get; set; }
        IEvaluateModelManagedF EvaluateModel { get; set; }
    }
}