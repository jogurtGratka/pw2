﻿using Kwiatkowski.MNIST.Interfaces;
using System.Collections.Generic;

namespace Kwiatkowski.MNIST.OneHiddenDao.BO
{
    public class Image : IImage
    {
        public int ImageID { get; set; }
        public int? Evaluate { get; set; }
        public int Label { get; set; }
        public List<float> Feature { get; set; }
    }
}