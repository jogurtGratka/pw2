﻿using Kwiatkowski.MNIST.Interfaces;
using Microsoft.MSR.CNTK.Extensibility.Managed;

namespace Kwiatkowski.MNIST.OneHiddenDao.BO
{
    public class Model : IModel
    {
        public string Name { get; set; }
        public IEvaluateModelManagedF EvaluateModel { get; set; }
    }
}