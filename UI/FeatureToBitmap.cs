﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Data;
using System.Windows.Media.Imaging;
using Color = System.Drawing.Color;


namespace Kwiatkowski.MNIST.UI
{
    public class FeatureToBitmap : IValueConverter
    {
        public object Convert(object value, Type targetType, object paramete, System.Globalization.CultureInfo culture)
        {
            if (Object.ReferenceEquals(null, value))
                return null;
            List<float> feature = (List<float>) value;

            int featureSize = 28;
            int bitmapSize = 56;
            int step = bitmapSize / featureSize;

            Bitmap bitmap = new Bitmap(bitmapSize, bitmapSize);

            using (Graphics gc = Graphics.FromImage(bitmap))
            {
                for (int i = 0; i < featureSize; ++i)
                    for (int j = 0; j < featureSize; ++j)
                    {
                        int v = 255 - (int) feature[i * featureSize + j];
                        gc.FillRectangle(new SolidBrush(Color.FromArgb(v, v, v)),
                            new Rectangle(j * step, i * step, step, step));
                    }
            }

            using (MemoryStream stream = new MemoryStream())
            {
                bitmap.Save(stream, System.Drawing.Imaging.ImageFormat.Bmp);
                stream.Position = 0;
                BitmapImage bitmapimage = new BitmapImage();
                bitmapimage.BeginInit();
                bitmapimage.StreamSource = stream;
                bitmapimage.CacheOption = BitmapCacheOption.OnLoad;
                bitmapimage.EndInit();

                return bitmapimage;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter,
            System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}