﻿using System;
using System.Windows.Data;

namespace Kwiatkowski.MNIST.UI
{
    public class NullableEvaluate : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.
            Globalization.CultureInfo culture)
        {
            return value ?? "Not evaluated.";
        }

        public object ConvertBack(object value, Type targetType, object parameter,
            System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}