﻿﻿using Kwiatkowski.MNIST.Interfaces;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Microsoft.MSR.CNTK.Extensibility.Managed;
using Color = System.Drawing.Color;


namespace Kwiatkowski.MNIST.UI
{
    public class ImageListViewModel : INotifyPropertyChanged
    {
        private const int BitmapSize = 28;

        private ObservableCollection<ImageViewModel> _images;
        private IDAO _dao;
        private readonly StrokeCollection _strokes;
        private ListCollectionView _view;
        private ImageViewModel _editedImage;
        private IModel _model;
        private int _correctlyEvaluated;
        private string _filterData;

        private RelayCommand _addImageCommand;
        private RelayCommand _groupImagesCommand;
        private RelayCommand _filterDataCommand;
        private RelayCommand _evaluateCommand;
        private RelayCommand _clearCommand;

        public event PropertyChangedEventHandler PropertyChanged;

        public int CorrectlyEvaluated
        {
            get { return _correctlyEvaluated; }
            set
            {
                _correctlyEvaluated = value;
                RaisePropertyChanged("CorrectlyEvaluated");
            }
        }

        public IModel Model
        {
            get { return _model; }
        }

        public StrokeCollection Signature
        {
            get { return _strokes; }
        }

        public ObservableCollection<ImageViewModel> Images
        {
            get { return _images; }
            set
            {
                _images = value;
                RaisePropertyChanged("Images");
            }
        }

        public ImageListViewModel()

        {
            try
            {
                Assembly dataSource = Assembly.UnsafeLoadFrom(Properties.Settings.Default.dataSource);
                var types = dataSource.GetTypes().Where(type => type.GetInterfaces().Contains(typeof(IDAO))).ToList();
                _dao = (IDAO) Activator.CreateInstance(types.First(), null);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Data.ToString());
                Environment.Exit(404);
            }


            _model = _dao.GetModel();
            _images = new ObservableCollection<ImageViewModel>();
            _view = (ListCollectionView) CollectionViewSource.GetDefaultView(_images);
            FilterData = "";
            GetAllImages();

            _addImageCommand = new RelayCommand(param => this.AddImage(), // bo argument to delegat Action
                param => this.CanAddImage());
            _filterDataCommand = new RelayCommand(param => this.DoFilterData());
            _groupImagesCommand = new RelayCommand(param => this.GroupByLabel());
            _clearCommand = new RelayCommand(param => this.ClearInkCanvas());
            _evaluateCommand = new RelayCommand(param => this.Evaluate(),
                param => this.CanEvaluate());

            _editedImage = new ImageViewModel(_dao.CreateNewImage());
            _strokes = new StrokeCollection();
            (_strokes as INotifyCollectionChanged).CollectionChanged += delegate { };
        }

        private void GetAllImages()
        {
            foreach (var image in _dao.GetAllImages())
            {
                _images.Add(new ImageViewModel(image));
            }
        }


        private void RaisePropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public RelayCommand AddImageCommand
        {
            get { return _addImageCommand; }
        }

        public ImageViewModel EditedImage
        {
            get { return _editedImage; }
            set
            {
                _editedImage = value;
                RaisePropertyChanged("EditedImage");
            }
        }

        private void AddImage()
        {
            int inkCanvasSize = 112;
            int step = inkCanvasSize / BitmapSize;
            InkCanvas inkCanvas = new InkCanvas();
            inkCanvas.Strokes = _strokes;

            StylusPointCollection stylusPointCollection = new StylusPointCollection();
            stylusPointCollection.Add(new StylusPoint(0, 0));
            stylusPointCollection.Add(new StylusPoint(152, 152));
            Stroke stroke = new Stroke(stylusPointCollection);
            DrawingAttributes attributes = new DrawingAttributes();
            attributes.Color = System.Windows.Media.Color.FromArgb(0,0,0,0);
            stroke.DrawingAttributes = attributes;
            inkCanvas.Strokes.Add(stroke);

            Rect rect =inkCanvas.Strokes.GetBounds();
            inkCanvas.Arrange(rect);
            inkCanvas.UpdateLayout();

            RenderTargetBitmap rtb = new RenderTargetBitmap(
                (int) inkCanvas.ActualWidth,
                (int) inkCanvas.ActualHeight,
                96d, 96d,
                PixelFormats.Default);
            rtb.Render(inkCanvas);

            MemoryStream stream = new MemoryStream();
            BitmapEncoder encoder = new BmpBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create(rtb));
            encoder.Save(stream);

            Bitmap bitmap = new Bitmap(stream);
            List<float> feature = new List<float>();
            for (int i = 0; i < inkCanvasSize; i += step)
            {
                for (int j = 0; j < inkCanvasSize; j += step)
                {
                    int sum = 0;
                    for (int k = 0; k < step; k++)
                    {
                        for (int l = 0; l < step; l++)
                        {
                            Color color = bitmap.GetPixel(j+l, i+k);
                            sum += color.B;
                        }
                    }
                    feature.Add( 255 - sum/16);
                }
            }

            IImage image = _dao.CreateNewImage();
            image.ImageID = _images.Count+1;
            image.Label = _editedImage.Label;
            image.Feature = feature;
            ImageViewModel imageViewModel = new ImageViewModel(image);
            _dao.AddImage(image);
            _images.Add(imageViewModel);
        }

        private bool CanAddImage()
        {
            if (EditedImage == null)
                return false;

            if (EditedImage.HasErrors)
                return false;

            return true;
        }

        public RelayCommand EvaluateCommand
        {
            get { return _evaluateCommand; }
        }

        private void Evaluate()
        {
            string outputLayerName;
            List<float> outputs;
            var inDimension = _model.EvaluateModel.GetNodeDimensions(NodeGroup.Input);
            List<float> features = new List<float>();
            int correctlyEvaluated = 0;
            foreach (var image in _images)
            {
                features = image.Feature;
                var dict = new Dictionary<string, List<float>>();
                dict.Add(inDimension.First().Key, features);
                var inputs = dict;
                var outDimension = _dao.GetModel().EvaluateModel.GetNodeDimensions(NodeGroup.Output);
                outputLayerName = outDimension.First().Key;
                outputs = _model.EvaluateModel.Evaluate(inputs, outputLayerName);
                var max = outputs.Select((value, index) => new {value, index})
                    .OrderByDescending(vi => vi.value)
                    .First();
                image.Evaluate = max.index;
                if (max.index == image.Label)
                {
                    ++correctlyEvaluated;
                }
            }
            CorrectlyEvaluated = correctlyEvaluated;
        }

        private bool CanEvaluate()
        {
            if (_images.Count == 0)
                return false;

            return true;
        }

        public RelayCommand ClearCommand
        {
            get { return _clearCommand; }
        }

        private void ClearInkCanvas()
        {
            Signature.Clear();
        }

        public RelayCommand FilterDataCommand
        {
            get { return _filterDataCommand; }
        }

        public string FilterData
        {
            get { return _filterData; }
            set
            {
                _filterData = value;
                RaisePropertyChanged("FilterData");
            }
        }
        private void DoFilterData()
        {
            int FilterDataInt = 0;
            if (Int32.TryParse(FilterData, out FilterDataInt))
            {
                if (FilterDataInt >= 0 && FilterDataInt <= 9)
                {
                    _view.Filter = (c) => ((ImageViewModel) c).Label.Equals(FilterDataInt);
                    return;
                }
            }
            _view.Filter = null;
        }

        public RelayCommand GroupImagesCommand
        {
            get { return _groupImagesCommand; }
        }

        private void GroupByLabel()
        {
            _view.SortDescriptions.Add(new SortDescription("Label", ListSortDirection.Ascending));
            _view.GroupDescriptions.Add(new PropertyGroupDescription("Label"));
        }
    }
}