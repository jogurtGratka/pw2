﻿using Kwiatkowski.MNIST.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace Kwiatkowski.MNIST.UI
{
    public class ImageViewModel : INotifyPropertyChanged, INotifyDataErrorInfo
    {
        private IImage _image;

        private Dictionary<string, List<string>> _validationErrors =
            new Dictionary<string, List<string>>();

        public event PropertyChangedEventHandler PropertyChanged;
        public event EventHandler<DataErrorsChangedEventArgs> ErrorsChanged;

        public ImageViewModel(IImage image)
        {
            _image = image;
            Validate();
        }

        public int ImageID
        {
            get { return _image.ImageID; }
            set
            {
                _image.ImageID = value;
                RaisePropertyChanged("ImageID");
            }
        }

        public int? Evaluate
        {
            get { return _image.Evaluate; }
            set
            {
                _image.Evaluate = value;
                RaisePropertyChanged("Evaluate");
            }
        }

        public int Label
        {
            get { return _image.Label; }
            set
            {
                _image.Label = value;
                RaisePropertyChanged("Label");
            }
        }

        public List<float> Feature
        {
            get { return _image.Feature; }
            set
            {
                _image.Feature = value;
                RaisePropertyChanged("Feature");
            }
        }

        private void RaisePropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
                Validate();
            }
        }

        public System.Collections.IEnumerable GetErrors(string propertyName)
        {
            if (string.IsNullOrEmpty(propertyName) || !_validationErrors.ContainsKey(propertyName))
                return null;

            return _validationErrors[propertyName];
        }

        public bool HasErrors
        {
            get { return _validationErrors.SelectMany(x => x.Value).ToList().Count > 0; }
        }

        public void Validate()
        {
            List<string> errors = new List<string>();

            if (this.Label > 9)
            {
                errors.Add("Label cannot be higher than 9");
            }

            if (this.Label < 0)
            {
                errors.Add("Label cannot be lower than 0");
            }
            _validationErrors["Label"] = errors;
        }
    }
}